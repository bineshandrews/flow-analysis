all:
	g++ -c libs/tree.cpp -o bin/objs/tree.o
	g++ -c libs/dllist.cpp -o bin/objs/dllist.o
	g++ -c src/pcap_parser.cpp -o bin/objs/pcap_parser.o
	g++ -c src/classify.cpp -o bin/objs/classify.o
	g++  bin/objs/tree.o bin/objs/dllist.o bin/objs/pcap_parser.o bin/objs/classify.o -o bin/pcap_classify.out

clean:
	rm -f bin/objs/*.*
	rm -f bin/*.*
