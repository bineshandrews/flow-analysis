#ifndef __DATA_TYPES_GENERIC_H__
#define __DATA_TYPES_GENERIC_H__

#include <stdint.h>

#define MIN(__x__, __y__) (__x__<__y__?__x__:__y__)
#define MAX(__x__, __y__) (__x__>__y__?__x__:__y__) 

#define ABS(x) (((x)>0)?(x):(-(x)))

#define OK 0 
#define ERROR -1

typedef int32_t STATUS;

enum BOOL { FALSE, TRUE };
enum DIRECTION { LEFT, RIGHT };

#endif
