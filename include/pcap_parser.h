#ifndef __PCAP_PARSER_H__
#define __PCAP_PARSER_H__

#include "commondefs.h"

#define EH_PREAMBLE_LEN 0
#define EH_MAC_ADDR_LEN 6

#define PCAP_NTYPE_ETHERNET 1
#define PCAP_NTYPE_PRISM 119

#define PKT_TYPE_ETH  0x1
#define PKT_TYPE_PRISM 0x2
#define PKT_TYPE_VLAN_TAG  0x4
#define PKT_TYPE_IP   0x10
#define PKT_TYPE_IPV6 0x100
#define PKT_TYPE_TCP  0x1000
#define PKT_TYPE_TCP_START  0x3000
#define PKT_TYPE_UDP  0x10000

#define ETH_HDR_TYPE_IP 0x800
#define ETH_HDR_TYPE_ARP 0x806
#define ETH_HDR_TYPE_VLAN_TAG 0x8100

#define IEEE_FTYPE_DATA 0x2
#define IEEE_80211_FTYPE_MGMT 0

#define IEEE_80211_FSTYPE_BEACON 8

#define IP_HDR_TYPE_ICMP 0x01
#define IP_HDR_TYPE_IGMP 0x02
#define IP_HDR_TYPE_IP   0x04
#define IP_HDR_TYPE_TCP  0x06
#define IP_HDR_TYPE_UDP  0x11
#define IP_HDR_TYPE_IPV6 0x29

#define GENERIC_ERROR(error) ((error) < 0 ? -1 : (error))

typedef int32_t FileDesc;
typedef uint8_t (MACADDR)[EH_MAC_ADDR_LEN];
typedef uint32_t IPADDR;
typedef uint16_t PORT;

/* The following struct definition is copied from wireshark developer page */
typedef struct  
{
	uint32_t magic_number;   /* magic number */
	uint16_t version_major;  /* major version number */
	uint16_t version_minor;  /* minor version number */
	int32_t  thiszone;       /* GMT to local correction */
	uint32_t sigfigs;        /* accuracy of timestamps */
	uint32_t snaplen;        /* max length of captured packets, in octets */
	uint32_t network;        /* data link type */

} PCAP_GLOBAL_HDR;


/* The following struct definition is copied from wireshark developer page */
typedef struct 
{
	uint32_t ts_sec;         /* timestamp seconds */
	uint32_t ts_usec;        /* timestamp microseconds */
	uint32_t incl_len;       /* number of octets of packet saved in file */
	uint32_t orig_len;       /* actual length of packet */

	/* Extra field added */
	uint8_t data[0];
} PCAP_PKT_HDR;

typedef struct
{
	//uint8_t  preamble[EH_PREAMBLE_LEN];
	MACADDR  dest_addr;
	MACADDR  src_addr;
	uint16_t type_len;
	uint8_t  data[0];         /* place holder for data + pad */

} PCAP_ETH_HDR;

typedef struct 
{
  uint32_t did;
  uint16_t status;
  uint16_t len;
  uint32_t data;
} PCAP_PRISM_VALUE;

typedef struct 
{
  uint32_t msgcode;
  uint32_t msglen;
  int8_t devname[16];
  PCAP_PRISM_VALUE hosttime;
  PCAP_PRISM_VALUE mactime;
  PCAP_PRISM_VALUE channel;
  PCAP_PRISM_VALUE rssi;
  PCAP_PRISM_VALUE sq;
  PCAP_PRISM_VALUE signal;
  PCAP_PRISM_VALUE noise;
  PCAP_PRISM_VALUE rate;
  PCAP_PRISM_VALUE istx;
  PCAP_PRISM_VALUE frmlen;
} PCAP_PRISM_HDR;

typedef struct
{
	//FC
	uint8_t version : 2;
	uint8_t type : 2;
	uint8_t subtype : 4;
	uint8_t flags;
	uint16_t duration;
	MACADDR dest;
	MACADDR bssid;
	MACADDR src;
	uint16_t frag_seq_num; // combined; split if necessary	
} PCAP_IEEE80211_FC;

typedef struct
{
	//LLC
	uint8_t dsap;
	uint8_t ssap;
	uint8_t control;
	uint8_t org_code[3];
	uint16_t llc_type;
} PCAP_IEEE80211_LLC;

typedef struct
{
	uint8_t  hdr_len : 4;
	uint8_t  version : 4;
	uint8_t  tos;
	uint16_t total_len;
	uint16_t ident;
	uint16_t frag_offset : 13;
	uint16_t flags : 3;
	uint8_t  ttl;
	uint8_t  protocol;
	uint16_t hdr_cksum;
	IPADDR   src_addr;
	IPADDR   dst_addr;
	//uint8_t  options[0];        /* placeholder for IP options */
	uint8_t  data[0];

} PCAP_IP_HDR;

typedef struct
{
	PORT     src_port;
	PORT     dst_port;
	uint32_t seq_num;
	uint32_t ack_num;
	uint8_t  rsvd1 : 4;
	uint8_t  data_offset : 4;
	uint8_t  flags_fin : 1;
	uint8_t  flags_syn : 1;
	uint8_t  flags_rst : 1;
	uint8_t  flags_psh : 1;
	uint8_t  flags_ack : 1;
	uint8_t  flags_urg : 1;
	uint8_t  rsvd2 : 2;
	uint16_t window;
	uint16_t cksum;
	uint16_t urg_ptr;	
	//uint8_t  options[0];
	uint8_t  data[0];

} PCAP_TCP_HDR;

typedef struct
{
	PORT     src_port;
	PORT     dst_port;
	uint16_t length;
	uint16_t cksum;
	uint8_t  data[0];

} PCAP_UDP_HDR;

typedef struct
{
	uint32_t type;
	uint32_t packet_id;
	PCAP_PKT_HDR *pktHdr;
	PCAP_ETH_HDR *eth_hdr;
	PCAP_IEEE80211_FC *l2_hdr;
	PCAP_IP_HDR  *ip_hdr;
	PCAP_TCP_HDR *tcp_hdr;
	PCAP_UDP_HDR *udp_hdr;
	void *data;

} PCAP_PKT_PARSER;

class CPcapParser
{
	FileDesc pcapFd;
	PCAP_GLOBAL_HDR *pcapGlobalHdr; 

	// Map the input file to memory for faster access to the contents
	int8_t *pcapFilePtr;
	int8_t *pcapLastByte;
	uint32_t pcapFileSize;

	// A simple bump allocator to speed up dynamic memory allocation
	int8_t *pcapDynMem;
	int8_t *pcapDynMemFreePtr;
	uint32_t pcapDynMemSizeRemain;
  public:
    CPcapParser();
	~CPcapParser();
	STATUS openPcap(char *filename);
	void closePcap(void);
	PCAP_GLOBAL_HDR *getGlobalHdr(void);
	STATUS readNextPkt(void **buffer, int32_t *buflen);
	STATUS parsePkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);
	STATUS parseEthPkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);
	STATUS parsePrismPkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);
	STATUS parseIpPkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);
	STATUS parseUdpPkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);
	STATUS parseTcpPkt(void *buffer, uint32_t len, PCAP_PKT_PARSER *result);

	void *allocateMem(int32_t size);
};

void printGlobalHdr(PCAP_GLOBAL_HDR *hdr);
void printMacAddr(MACADDR mac);
void printIPAddr(IPADDR ip);

#endif
