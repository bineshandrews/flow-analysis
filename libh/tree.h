#ifndef __TREE_H__
#define __TREE_H__

#include "../include/commondefs.h"

typedef struct TreeNode
{
    struct TreeNode **childList;
    struct TreeNode *parent;

    TreeNode()
    {
        childList = (TreeNode **)NULL;
	parent = (TreeNode *)NULL;
    }
} TREE_NODE; 

typedef TREE_NODE *PTR_TREE_NODE;


typedef struct TreeCB
{
    uint32_t nodeCount;
    PTR_TREE_NODE head;

    TreeCB()
    {
        nodeCount = 0;
	head = (PTR_TREE_NODE)NULL;
    }
} TREE_CB; 

typedef TREE_CB *PTR_TREE_CB;

typedef struct BSTreeNode
{
    struct BSTreeNode *child[2];
    uint32_t height;

    BSTreeNode()
    {
        height = 0;
	child[LEFT] = child[RIGHT] = (BSTreeNode *)NULL;
    }
} BS_TREE_NODE;

typedef BS_TREE_NODE *PTR_BS_TREE_NODE;

typedef struct BSTreeCB
{
    uint32_t nodeCount;
    PTR_BS_TREE_NODE head;

    BSTreeCB()
    {
        nodeCount = 0;
		head = (PTR_BS_TREE_NODE)NULL;
    }
} BS_TREE_CB;

typedef BS_TREE_CB *PTR_BS_TREE_CB;

typedef enum {AVL_ROTATE_SINGLE, AVL_ROTATE_DOUBLE} ROTATE_TYPE;

typedef STATUS (*COMPAREFN)(void *, void *);
typedef void (*TRAVERSE_ACTION)(void *);

class CBSTree
{
    private:

    BS_TREE_CB controlBlock;
    COMPAREFN sortFn;
    TRAVERSE_ACTION travActn;
    BOOL avl;

    STATUS insertNodeInternal(PTR_BS_TREE_NODE &, PTR_BS_TREE_NODE);
    PTR_BS_TREE_NODE deleteNodeInternal(PTR_BS_TREE_NODE &, PTR_BS_TREE_NODE);
    PTR_BS_TREE_NODE findNodeInternal(PTR_BS_TREE_NODE, PTR_BS_TREE_NODE);
    
    PTR_BS_TREE_NODE deleteFirstInternal(PTR_BS_TREE_NODE &);
    PTR_BS_TREE_NODE deleteLastInternal(PTR_BS_TREE_NODE &);

    PTR_BS_TREE_NODE rotateForAVLness(DIRECTION dir, ROTATE_TYPE, 
                                      PTR_BS_TREE_NODE); 

    void preOrderTraverseInternal(PTR_BS_TREE_NODE);
    void inOrderTraverseInternal(PTR_BS_TREE_NODE);
    void postOrderTraverseInternal(PTR_BS_TREE_NODE);
    
    uint32_t nodeCountInternal(PTR_BS_TREE_NODE);
    uint32_t treeHeightInternal(PTR_BS_TREE_NODE);

    public:

    CBSTree();
    CBSTree(BOOL);
    CBSTree(BOOL, COMPAREFN);
    CBSTree(BOOL, COMPAREFN, TRAVERSE_ACTION); 

	void initCB();
    void setSort(COMPAREFN);
    void setTraverse(TRAVERSE_ACTION);
	void setMode(BOOL);

    STATUS insertNode(PTR_BS_TREE_NODE);
    PTR_BS_TREE_NODE deleteNode(PTR_BS_TREE_NODE);
    PTR_BS_TREE_NODE findNode(PTR_BS_TREE_NODE);
    
    PTR_BS_TREE_NODE deleteFirst();
    PTR_BS_TREE_NODE deleteLast();

    PTR_BS_TREE_NODE findMin();
    PTR_BS_TREE_NODE findMax();
    PTR_BS_TREE_NODE findNode();

    uint32_t treeHeight(); //from root
    uint32_t nodeCount();

    int8_t getBalanceFactor(PTR_BS_TREE_NODE);

    void preOrderTraverse();
    void inOrderTraverse();
    void postOrderTraverse();
};

#endif
