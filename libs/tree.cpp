#include <stdio.h>
#include <iostream>
#include <stddef.h>
#include "../libh/tree.h"

using namespace std;

CBSTree :: CBSTree()
{
    avl = FALSE;
    setSort((COMPAREFN)NULL);
    setTraverse((TRAVERSE_ACTION)NULL);
}

CBSTree :: CBSTree(BOOL avlMode)
{
    avl = avlMode;
    setSort((COMPAREFN)NULL);
    setTraverse((TRAVERSE_ACTION)NULL);
}

CBSTree :: CBSTree(BOOL avlMode, COMPAREFN sFn)
{
    avl = avlMode;
    setSort(sFn);
    setTraverse((TRAVERSE_ACTION)NULL);
}

CBSTree :: CBSTree (BOOL avlMode, COMPAREFN sFn, TRAVERSE_ACTION tActn)
{
    avl = avlMode;
    setSort(sFn);
    setTraverse(tActn);
}

void CBSTree :: initCB()
{
	controlBlock.nodeCount = 0;
	controlBlock.head = (PTR_BS_TREE_NODE) NULL;	
}

void CBSTree :: setSort(COMPAREFN sFn)
{
    sortFn = sFn;
}

void CBSTree :: setTraverse(TRAVERSE_ACTION tAct)
{
    travActn = tAct;
}

void CBSTree :: setMode(BOOL avlMode)
{
	avl = avlMode;	
}

uint32_t CBSTree :: treeHeight()
{
    if(controlBlock.head != NULL) 
    	return treeHeightInternal(controlBlock.head);
}

uint32_t CBSTree :: treeHeightInternal(PTR_BS_TREE_NODE node)
{
    if(node == NULL) 
        return 0;

    return (1 + node->height);
}

int8_t CBSTree :: getBalanceFactor(PTR_BS_TREE_NODE node)
{
    int8_t balanceFactor;

    if(node == NULL)
        return 0;

    balanceFactor = treeHeightInternal(node->child[LEFT]) -
                    treeHeightInternal(node->child[RIGHT]);

    return balanceFactor;
}

uint32_t CBSTree :: nodeCount()
{
    return nodeCountInternal(controlBlock.head);
}

uint32_t CBSTree :: nodeCountInternal(PTR_BS_TREE_NODE node)
{
    if(node == NULL)
        return 0;
    else
    {
        return (1 + 
	        nodeCountInternal(node->child[LEFT]) + 
	        nodeCountInternal(node->child[RIGHT]));
    }
}

void CBSTree :: preOrderTraverse()
{
    if(travActn == NULL)
    {
        return;
    }

    preOrderTraverseInternal(controlBlock.head);
}

void CBSTree :: inOrderTraverse()
{
    if(travActn == NULL)
    {
        return;
    }

    inOrderTraverseInternal(controlBlock.head);
}

void CBSTree :: postOrderTraverse()
{
    if(travActn == NULL)
    {
        return;
    }

    postOrderTraverseInternal(controlBlock.head);
}

void CBSTree :: preOrderTraverseInternal(PTR_BS_TREE_NODE node)
{
    if(node == NULL)
        return;

    travActn(node);
    preOrderTraverseInternal(node->child[LEFT]);
    preOrderTraverseInternal(node->child[RIGHT]);
}

void CBSTree :: inOrderTraverseInternal(PTR_BS_TREE_NODE node)
{
    if(node == NULL)
        return;

    inOrderTraverseInternal(node->child[LEFT]);
    travActn(node);
    inOrderTraverseInternal(node->child[RIGHT]);
}

void CBSTree :: postOrderTraverseInternal(PTR_BS_TREE_NODE node)
{
    if(node == NULL)
        return;

    postOrderTraverseInternal(node->child[LEFT]);
    postOrderTraverseInternal(node->child[RIGHT]);
    travActn(node);
}

PTR_BS_TREE_NODE CBSTree :: findMin()
{
    PTR_BS_TREE_NODE ptr;

    ptr = controlBlock.head;

    while(ptr->child[LEFT] != NULL)
        ptr = ptr->child[LEFT];

    return ptr;
}

PTR_BS_TREE_NODE CBSTree :: findMax()
{
    PTR_BS_TREE_NODE ptr;

    ptr = controlBlock.head;

    while(ptr->child[RIGHT] != NULL)
        ptr = ptr->child[RIGHT];

    return ptr;
}

PTR_BS_TREE_NODE CBSTree :: findNode(PTR_BS_TREE_NODE criteria)
{
    if(criteria == NULL || sortFn == NULL)
        return NULL;

    return findNodeInternal(controlBlock.head, criteria);
}

STATUS CBSTree :: insertNode(PTR_BS_TREE_NODE insNode)
{
    if(insNode == NULL || sortFn == NULL)
        return ERROR;

    return insertNodeInternal(controlBlock.head, insNode);
}

PTR_BS_TREE_NODE CBSTree :: deleteNode(PTR_BS_TREE_NODE delNode)
{
    if(delNode == NULL || sortFn == NULL)
        return NULL;

    return deleteNodeInternal(controlBlock.head, delNode);
}

PTR_BS_TREE_NODE CBSTree :: rotateForAVLness(DIRECTION dir, ROTATE_TYPE type, 
                                             PTR_BS_TREE_NODE node)
{
    PTR_BS_TREE_NODE tmpNode;

    if(type == AVL_ROTATE_SINGLE)
    {
        tmpNode = node->child[dir];
        node->child[dir] = tmpNode->child[(dir+1)%2];
        tmpNode->child[(dir+1)%2] = node;

	node->height = MAX(treeHeightInternal(node->child[LEFT]),
	                    treeHeightInternal(node->child[RIGHT]));

        tmpNode->height = MAX(treeHeightInternal(tmpNode->child[LEFT]),
	                      treeHeightInternal(tmpNode->child[RIGHT]));

	return tmpNode;
    }
    else if(type == AVL_ROTATE_DOUBLE)
    {
        node->child[dir] = rotateForAVLness(DIRECTION((dir+1)%2), 
	                                    AVL_ROTATE_SINGLE, 
	                                    node->child[dir]);
	return rotateForAVLness(dir, AVL_ROTATE_SINGLE, node);
    }
    else
        return NULL;
}

PTR_BS_TREE_NODE CBSTree :: deleteFirst()
{
    if(controlBlock.head != NULL)
        return deleteFirstInternal(controlBlock.head);
    else
        return NULL;
}

PTR_BS_TREE_NODE CBSTree :: deleteLast()
{
    if(controlBlock.head != NULL)
        return deleteLastInternal(controlBlock.head);
    else
        return NULL;
}

PTR_BS_TREE_NODE CBSTree :: deleteFirstInternal(PTR_BS_TREE_NODE &refNode)
{
     PTR_BS_TREE_NODE tmpNode;

     if(refNode == NULL)
         return NULL;

     if(refNode->child[LEFT] != NULL)
     {
         tmpNode = deleteFirstInternal(refNode->child[LEFT]);
     }
     else
     {
         tmpNode = refNode;
	 refNode = refNode->child[RIGHT];
     }

     return tmpNode; 
}

PTR_BS_TREE_NODE CBSTree :: deleteLastInternal(PTR_BS_TREE_NODE &refNode)
{
     PTR_BS_TREE_NODE tmpNode;

     if(refNode == NULL)
         return NULL;

     if(refNode->child[RIGHT] != NULL)
     {
         tmpNode = deleteLastInternal(refNode->child[RIGHT]);
     }
     else
     {
         tmpNode = refNode;
	 refNode = refNode->child[LEFT];
     }

     return tmpNode; 
}

STATUS CBSTree :: insertNodeInternal(PTR_BS_TREE_NODE &refNode, 
                                    PTR_BS_TREE_NODE insNode)
{
    STATUS status;
    DIRECTION dir;
    int8_t balanceFactor;

#if 0
    int8_t balanceFactorInt;
#endif

    if(refNode == NULL)
    {
        insNode->child[LEFT] = insNode->child[RIGHT] = NULL;
	insNode->height = 0;
	refNode = insNode;
    }
    else
    {
        status = sortFn(insNode, refNode);

	if(status == 0)
	    return ERROR; //node already exists

        if(status < 0)
	    dir = LEFT;
	else 
	    dir = RIGHT;

	status = insertNodeInternal(refNode->child[dir], insNode);

	if(status == ERROR)
	    return status;

	if(avl == TRUE)
	{
	    balanceFactor = getBalanceFactor(refNode);

	    if(ABS(balanceFactor) >= 2)
	    {

#if 0
	       balanceFactorInt = getBalanceFactor(refNode->child[dir]);

	       if((balanceFactor > 0 && balanceFactorInt >= 0) ||
	          (balanceFactor < 0 && balanceFactorInt <= 0))
	        {
		    refNode = rotateForAVLNess(dir, AVL_ROTATE_SINGLE, refNode);
	        }
		else
		{
		    refNode = rotateForAVLNess(dir, AVL_ROTATE_DOUBLE, refNode);
		}
#endif

	        if(sortFn(insNode, refNode->child[dir]) < 0)
		{
		    refNode = rotateForAVLness(dir, 
		                           ROTATE_TYPE(AVL_ROTATE_SINGLE + dir),
					   refNode);
		}
		else
		{
		    refNode = rotateForAVLness(dir, 
		                      ROTATE_TYPE((AVL_ROTATE_DOUBLE + dir)%2),
				      refNode);
		}
	    }
	}
    }
	
    refNode->height = MAX(treeHeightInternal(refNode->child[LEFT]),
                          treeHeightInternal(refNode->child[RIGHT]));

    return OK;
}


PTR_BS_TREE_NODE CBSTree :: deleteNodeInternal(PTR_BS_TREE_NODE &refNode, 
                                    PTR_BS_TREE_NODE delNode)
{
    PTR_BS_TREE_NODE tmpNode = (PTR_BS_TREE_NODE)NULL;
    STATUS status;
    DIRECTION dir, otherDir;
    int32_t balanceFactor, balanceFactorInt;

    if(refNode == NULL)
    {
        return NULL;
    }

    status = sortFn(delNode, refNode);

    if(status == 0)
    {
        // element found
	
	if(refNode->child[LEFT] == NULL && refNode->child[RIGHT] == NULL)
	{
	    tmpNode = refNode;
	    refNode = NULL;
	}

	else if(refNode->child[LEFT] == NULL)
	{
	    tmpNode = refNode;
	    refNode = refNode->child[RIGHT];
	}

	else if(refNode->child[RIGHT] == NULL)
	{
	    tmpNode = refNode;
	    refNode = refNode->child[LEFT];
	}
	else
	{
	    /* interchange.. with min on right subtree of max on left subtree*/
	    /* delete the node... */

	    tmpNode = refNode;
	    refNode = deleteFirstInternal(refNode->child[RIGHT]);
            *refNode =  *tmpNode;
	}
    }

    if(status != 0)  
    {
        if(status < 0)
	    dir = LEFT;
	else
	    dir = RIGHT;
        
	tmpNode = deleteNodeInternal(refNode->child[dir], delNode);

	refNode->height = MAX(treeHeightInternal(refNode->child[LEFT]),  
	                      treeHeightInternal(refNode->child[RIGHT]));


	if(avl == TRUE)
	{
	    balanceFactor = getBalanceFactor(refNode);

            if(ABS(balanceFactor) >= 2)
	    {
	       otherDir = DIRECTION((dir+1)%2);
	       balanceFactorInt = getBalanceFactor(refNode->child[otherDir]);

	       if((balanceFactor > 0 && balanceFactorInt >= 0) ||
	          (balanceFactor < 0 && balanceFactorInt <= 0))
		{
		    refNode = rotateForAVLness(otherDir, 
		                               AVL_ROTATE_SINGLE, 
					       refNode);
		}
		else
		{
		    refNode = rotateForAVLness(otherDir, 
		                               AVL_ROTATE_DOUBLE,
				               refNode);
		}
	    }
	}
    }

    return tmpNode;
}

PTR_BS_TREE_NODE CBSTree :: findNodeInternal(PTR_BS_TREE_NODE refNode, 
                                  PTR_BS_TREE_NODE criteria)
{
    STATUS retVal;

    if(refNode == NULL)
    {
        return NULL;
    }
    
    retVal = sortFn(refNode, criteria);
    if(retVal == 0)
    {
        //got the required one
        return refNode;
    }
    else if(retVal > 0)
        return findNodeInternal(refNode->child[LEFT], criteria);
    else
        return findNodeInternal(refNode->child[RIGHT], criteria);
}
