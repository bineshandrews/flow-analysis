#include <cstdio>
#include <ctime>
#include <cstring>
#include <math.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include "../include/pcap_parser.h"
#include "../libh/tree.h"
#include "../libh/dllist.h"

#define PRINT_MASK 0x0
#define PCAP_PRINT(level) if(level & PRINT_MASK) printf

#define NEW_FLOW_ON_LONG_IDLE 1
#define TCP_CLASSIFY_NEW_FLOWS_USING_SYN_ACK 1

#define TCP_FLOW_STATE_NONE 0x0
#define TCP_FLOW_STATE_UNKNOWN 0x1
#define TCP_FLOW_STATE_STARTING 0x2
#define TCP_FLOW_STATE_ONGOING 0x3
#define TCP_FLOW_STATE_FINISHING 0x4

CPcapParser parser;
int32_t globalCounter;
int32_t selectedFlowId;
bool stillSearching;
bool csvMode;
int32_t eligibilityCriteria;
char outputFileName[200] = "testOut.pcap";
FILE *outputFile;

/* For US/DS flow classification. The SIGCOMM traces specifies the following source addresses */
IPADDR srcAddr[] = {htonl(0x1A0C0000), htonl(0x1A020000)}; //big endian format
IPADDR srcAddrMask[] = {htonl(0xFFFF0000), htonl(0xFFFF0000)}; //big endian format
int32_t numSrcAddr = 2;


typedef struct
{
        BS_TREE_NODE flow;
        CDLList flowList;
        PCAP_PKT_PARSER parser;
} PCAP_FLOWID_TREE_NODE;

typedef struct
{
        /* List of lists */
        DLL_NODE node;
        uint32_t flow_id;
		uint32_t tcpFlowState;
        CDLList flows;
} PCAP_FLOW_LIST_NODE;

typedef struct
{
        DLL_NODE node;
        PCAP_PKT_PARSER parser;
} PCAP_FLOW_NODE;

typedef struct
{
		BS_TREE_NODE orderedFlowId;
		uint32_t flow_id;
		PCAP_FLOW_LIST_NODE *node;
} PCAP_ORDERED_FLOWID_TREE_NODE;

void printGlobalHdr(PCAP_GLOBAL_HDR *hdr)
{
	if(hdr == NULL)
		return;

	PCAP_PRINT(0x2)("Global PCAP header info:\n");
	PCAP_PRINT(0x2)("\tMagic Number       : %X\n", hdr->magic_number);
	PCAP_PRINT(0x2)("\tMajor version      : %d\n", hdr->version_major);
	PCAP_PRINT(0x2)("\tMinor version      : %d\n", hdr->version_minor);
	PCAP_PRINT(0x2)("\tTime zone          : %d\n", hdr->thiszone);
	PCAP_PRINT(0x2)("\tTimestamp accuracy : %u\n", hdr->sigfigs);
	PCAP_PRINT(0x2)("\tMax packet length  : %u\n", hdr->snaplen);
	PCAP_PRINT(0x2)("\tNetwork type       : %u\n", hdr->network);
}

void printMacAddr(MACADDR mac)
{
	assert(mac != NULL);
	PCAP_PRINT(0x1)("%x%x.%x%x.%x%x", 
			mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void printIpAddr(IPADDR ip)
{
	uint32_t temp;

	temp = ntohl(ip);
	PCAP_PRINT(0x1)("%u.%u.%u.%u", 
			(temp >> 24) & 0xFF, (temp >> 16) & 0xFF, 
			(temp >> 8) & 0xFF, (temp) & 0xFF);
}

void formatIpAddr(IPADDR ip, char *ipStr)
{
	uint32_t temp;

	assert(ipStr != NULL);
	
	temp = ntohl(ip);
	sprintf(ipStr, "%u.%u.%u.%u", 
			(temp >> 24) & 0xFF, (temp >> 16) & 0xFF, 
			(temp >> 8) & 0xFF, (temp) & 0xFF);
			
	return;
}


int32_t flowDetectCompare(void *ptr1, void *ptr2)
{
	PCAP_PKT_PARSER *a = &((PCAP_FLOWID_TREE_NODE *)ptr1)->parser;
	PCAP_PKT_PARSER *b = &((PCAP_FLOWID_TREE_NODE *)ptr2)->parser;
	PORT srcp1, srcp2, dstp1, dstp2;
	IPADDR srcip1, srcip2, dstip1, dstip2;
	uint32_t mask1, mask2;

	mask1 = a->type & (PKT_TYPE_TCP | PKT_TYPE_UDP);
	mask2 = b->type & (PKT_TYPE_TCP | PKT_TYPE_UDP);

	if(!(mask1|mask2)) //Both packets are not TCP/UDP
		return 0;

	if(mask1 > mask2)
		return 1;

	else if(mask1 < mask2)
		return -1;

	//Types are same.. check ports and IP address
	// port has more weightage; src port more among src and dest

	srcip1 = ntohl(a->ip_hdr->src_addr);
	srcip2 = ntohl(b->ip_hdr->src_addr);
	dstip1 = ntohl(a->ip_hdr->dst_addr);
	dstip2 = ntohl(b->ip_hdr->dst_addr);

	if(a->type & PKT_TYPE_TCP)
	{
		srcp1 = ntohs(a->tcp_hdr->src_port);
		srcp2 = ntohs(b->tcp_hdr->src_port);
		dstp1 = ntohs(a->tcp_hdr->dst_port);
		dstp2 = ntohs(b->tcp_hdr->dst_port);
	}
	else if(a->type & PKT_TYPE_UDP)
	{
		srcp1 = ntohs(a->udp_hdr->src_port);
		srcp2 = ntohs(b->udp_hdr->src_port);
		dstp1 = ntohs(a->udp_hdr->dst_port);
		dstp2 = ntohs(b->udp_hdr->dst_port);
	}

	if(srcip1 == srcip2 && dstip1 == dstip2 && srcp1 == srcp2 && dstp1 == dstp2)
	{
		return 0;
	}

	if(srcip1 == dstip2 && dstip1 == srcip2 && srcp1 == dstp2 && dstp1 == srcp2)
	{
		return 0;
	}

	if(MAX(srcp1, dstp1) < MAX(srcp2, dstp2))
		return -1;

	if(MAX(srcp1, dstp1) > MAX(srcp2, dstp2))
		return 1;

	if(MIN(srcp1, dstp1) < MIN(srcp2, dstp2))
		return -1;

	if(MIN(srcp1, dstp1) > MIN(srcp2, dstp2))
		return 1;

	if(MAX(srcip1, dstip1) < MAX(srcip2, dstip2))
		return -1;

	if(MAX(srcip1, dstip1) > MAX(srcip2, dstip2))
		return 1;

	if(MIN(srcip1, dstip1) < MIN(srcip2, dstip2))
		return -1;

	if(MIN(srcip1, dstip1) > MIN(srcip2, dstip2))
		return 1;

	return 0;
}
int32_t orderedFlowDetectCompare(void *ptr1, void *ptr2)
{
	//Program should make sure that these pointers are not null
	uint32_t flowId1 = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr1)->flow_id;
	uint32_t flowId2 = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr2)->flow_id;
	
	if(flowId1 > flowId2)
		return 1;
	else if(flowId1 < flowId2)
		return -1;
	else 
		return 0;
}

void treeVerify(void *ptr)
{
	PCAP_FLOWID_TREE_NODE *a = (PCAP_FLOWID_TREE_NODE *)ptr;
	PCAP_FLOW_LIST_NODE *b;
	int32_t count = 0;

	b = (PCAP_FLOW_LIST_NODE *)a->flowList.getHead();

	while(b != NULL)
	{
		count += b->flows.getListSize();
		b = (PCAP_FLOW_LIST_NODE *)a->flowList.getNext((DLL_NODE *)b);
	}

	globalCounter += count;
}

void countEligibleFlows(void *ptr)
{
	PCAP_FLOW_LIST_NODE *a = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr)->node;

	if(a->flows.getListSize() >= eligibilityCriteria /*&& a->tcpFlowState == TCP_FLOW_STATE_ONGOING*/)
		globalCounter++;
}

void printEligibleFlows(void *ptr)
{
	PCAP_FLOW_LIST_NODE *a = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr)->node;

	if(a->flows.getListSize() >= eligibilityCriteria /*&& b->tcpFlowState == TCP_FLOW_STATE_ONGOING*/)
	{
		printf("%d ", a->flow_id);
	}
}

void writeFlowToFile(void *ptr)
{
	PCAP_FLOW_LIST_NODE *b = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr)->node;
	PCAP_FLOW_NODE *c;
	int32_t fd = -1;


	fd = open(outputFileName, O_RDWR| O_CREAT, 0644);

	if(fd == -1)
	{
		printf("Failed to open file..\n");
		return;
	}

	write(fd, parser.getGlobalHdr(), sizeof(PCAP_GLOBAL_HDR));

	c = (PCAP_FLOW_NODE *)b->flows.getHead();	

	while(c != NULL)
	{
		//Write to file c->parser
		write(fd, c->parser.pktHdr, c->parser.pktHdr->incl_len + sizeof(PCAP_PKT_HDR));

		c = (PCAP_FLOW_NODE *)b->flows.getNext((DLL_NODE *)c);
	}

	if(fd != -1)
		close(fd);
}

void printFlowStats(void *ptr)
{
	PCAP_FLOW_LIST_NODE *b = ((PCAP_ORDERED_FLOWID_TREE_NODE *)ptr)->node;
	PCAP_FLOW_NODE *c, *d;
	int32_t count = 0;
	int32_t fd = -1;
	int32_t secs, nsecs;
	double tim1, tim2;
	double lastSrc = 0, lastDst = 0;
	double avgDiff = -2.000000, avgDiffSrc = -2.000000, avgDiffDst = -2.000000;
	double avgBlkDiff = -2.000000, avgBlkDiffSrc = -2.000000, avgBlkDiffDst = -2.000000;
	double avgDiffSq = -2.000000, avgDiffSrcSq = -2.000000, avgDiffDstSq = -2.000000;
	double avgBlkDiffSq = -2.000000, avgBlkDiffSrcSq = -2.000000, avgBlkDiffDstSq = -2.000000;	
	int32_t countDiff = -1, countDiffSrc = -1, countDiffDst = -1;
	int32_t countBlkDiff = -1, countBlkDiffSrc = -1, countBlkDiffDst = -1;
	uint32_t pktSizeTotal = 0, pktSizeTotalSrc = 0, pktSizeTotalDst = 0; //putting int32_t for platform independence
	bool foundSrc = false, foundDst = false;
	int32_t i, j;
	char ipStr1[16], ipStr2[16];
	uint16_t port1, port2;
	time_t startSecs, endSecs;
	uint32_t startUSecs, endUSecs;
	char *typeStr;
	char startTimeStr[50], endTimeStr[50];
	uint8_t tos;
	uint16_t higherLayerProto = 0;
	
	if(!stillSearching)
		return;
	
	//assert(sizeof(uint64_t) == 8); //enable this if an int64_t variable is defined!

	if((selectedFlowId == 0xFFFFFFFE && b->flows.getListSize() >= eligibilityCriteria) || b->flow_id == selectedFlowId)
	{
		if(selectedFlowId != 0xFFFFFFFE)
			stillSearching = false;

		// Calculate mean inter arrival time for packet
		c = (PCAP_FLOW_NODE *)b->flows.getHead();	
		d = (PCAP_FLOW_NODE *)b->flows.getTail();	

		startSecs = (time_t)c->parser.pktHdr->ts_sec;
		endSecs = (time_t)d->parser.pktHdr->ts_sec;
		startUSecs = c->parser.pktHdr->ts_usec;
		endUSecs = d->parser.pktHdr->ts_usec;
		
		strcpy(&startTimeStr[1], ctime(&startSecs));
		strcpy(&endTimeStr[1], ctime(&endSecs));
		
		for(i = strlen(&startTimeStr[1])- 3; i > 0; i--)
		{
			if(startTimeStr[i] == ' ')
			{
				startTimeStr[i] = '\0';
				startTimeStr[0] = startTimeStr[i+1];
				startTimeStr[1] = startTimeStr[i+2];
				startTimeStr[2] = startTimeStr[i+3];
				startTimeStr[3] = startTimeStr[i+4];
				break;
			}
		}

		for(i = strlen(&endTimeStr[1])- 3; i > 0; i--)
		{
			if(endTimeStr[i] == ' ')
			{
				endTimeStr[i] = '\0';
				endTimeStr[0] = endTimeStr[i+1];
				endTimeStr[1] = endTimeStr[i+2];
				endTimeStr[2] = endTimeStr[i+3];
				endTimeStr[3] = endTimeStr[i+4];
				break;
			}
		}
		
		tos = c->parser.ip_hdr->tos;
		formatIpAddr(c->parser.ip_hdr->src_addr, ipStr1);
		formatIpAddr(c->parser.ip_hdr->dst_addr, ipStr2);

		if(c->parser.type & PKT_TYPE_UDP)
		{
			typeStr = (char *)"UDP";
			port1 = ntohs(c->parser.udp_hdr->src_port);
			port2 = ntohs(c->parser.udp_hdr->dst_port);			
		}
		else
		{
			typeStr = (char *)"TCP";
			port1 = ntohs(c->parser.tcp_hdr->src_port);
			port2 = ntohs(c->parser.tcp_hdr->dst_port);			
		}
		
		if(port1 < 1024) higherLayerProto = port1;
		else if(port2 < 1024) higherLayerProto = port2;
		

		if(c != NULL && b->flows.getNext((DLL_NODE *)c) != NULL)
		{				
			while(c != NULL)
			{
				tim2 = c->parser.pktHdr->ts_sec + c->parser.pktHdr->ts_usec/1000000.000000;
				
				if(avgDiff < -1)
				{
					avgDiff = 0.000000;
					avgBlkDiff = 0.000000;
					countDiff=0;
					countBlkDiff = 0;
				}
				else
				{
					avgDiff += (tim2 - tim1);
					countDiff++;
					
					if((tim2 - tim1) > 1.000000)
					{
						avgBlkDiff += (tim2-tim1);
						countBlkDiff++;
					}
				}
				
				foundSrc = false;
				foundDst = false;
				for(i = 0; i < numSrcAddr; i++)
				{
					if((c->parser.ip_hdr->src_addr & srcAddrMask[i]) == srcAddr[i])
					{
						
						foundSrc = true;
					}
					if((c->parser.ip_hdr->dst_addr & srcAddrMask[i]) == srcAddr[i])
					{
						foundDst = true;
					}
				}
				
				if(foundSrc == true)
				{
					if(avgDiffSrc < -1)
					{
						avgDiffSrc = 0.000000;
						avgBlkDiffSrc = 0.000000;
						countDiffSrc = 0;
						countBlkDiffSrc = 0;
					}
					else
					{
						avgDiffSrc += (tim2 - lastSrc);
						countDiffSrc++;
						
						if((tim2-lastSrc) > 1.000000)
						{
							avgBlkDiffSrc += (tim2-lastSrc);
							countBlkDiffSrc++;
						}

					}
					lastSrc = tim2;
				}
				
				if(foundDst == true)
				{
					if(avgDiffDst < -1)
					{
						avgDiffDst = 0.000000;
						avgBlkDiffDst = 0.000000;
						countDiffDst = 0;
						countBlkDiffDst = 0;
					}
					else
					{
						avgDiffDst += (tim2 - lastDst);
						countDiffDst++;							
						
						if((tim2-lastDst) > 1.000000)
						{
							avgBlkDiffDst += (tim2-lastDst);
							countBlkDiffDst++;
						}
					}
					lastDst = tim2;
				}
					
				tim1 = tim2;
				c = (PCAP_FLOW_NODE *)b->flows.getNext((DLL_NODE *)c);
			}

			
			if(countDiff >= 1)
				avgDiff = avgDiff/countDiff;
			else
				avgDiff = 0.000000;

			if(countBlkDiff >= 1)
				avgBlkDiff = avgBlkDiff/countBlkDiff;
			else
				avgBlkDiff = 0.000000;
				
			if(countDiffDst >= 1)
				avgDiffDst = avgDiffDst/countDiffDst;
			else
				avgDiffDst = 0.000000;

			if(countDiffSrc >=1)
				avgDiffSrc = avgDiffSrc/countDiffSrc;
			else
				avgDiffSrc = 0.000000;

			if(countBlkDiffDst >= 1)
				avgBlkDiffDst = avgBlkDiffDst/countBlkDiffDst;
			else
				avgBlkDiffDst = 0.000000;

			if(countBlkDiffSrc >= 1)
				avgBlkDiffSrc = avgBlkDiffSrc/countBlkDiffSrc;
			else
				avgBlkDiffSrc = 0.000000;

			c = (PCAP_FLOW_NODE *)b->flows.getHead();	

			while(c != NULL)
			{
				tim2 = c->parser.pktHdr->ts_sec + c->parser.pktHdr->ts_usec/1000000.000000;
				
				if(avgDiffSq < -1)
				{
					avgDiffSq = 0.000000;
					avgBlkDiffSq = 0.000000;
				}
				else
				{
					avgDiffSq += ((avgDiff - (tim2 - tim1)) * (avgDiff - (tim2 - tim1)));	
					
					if((tim2-tim1) > 1.000000)
						avgBlkDiffSq += ((avgBlkDiff - (tim2 - tim1)) * (avgBlkDiff - (tim2 - tim1)));	
				}
					
				pktSizeTotal += c->parser.pktHdr->orig_len;
					
				foundSrc = false;
				foundDst = false;
				for(i = 0; i < numSrcAddr; i++)
				{
					if((c->parser.ip_hdr->src_addr & srcAddrMask[i]) == srcAddr[i])
					{
						foundSrc = true;
					}
					if((c->parser.ip_hdr->dst_addr & srcAddrMask[i]) == srcAddr[i])
					{
						foundDst = true;
					}
				}
					
				if(foundSrc == true)
				{
					if(avgDiffSrcSq < -1)
					{
						avgDiffSrcSq = 0.000000;
						avgBlkDiffSrcSq = 0.000000;
					}
					else
					{
						avgDiffSrcSq += ((avgDiffSrc - (tim2 - lastSrc)) * (avgDiffSrc - (tim2 - lastSrc)));
						
						if((tim2 - lastSrc) > 1.000000)
							avgBlkDiffSrcSq += ((avgBlkDiffSrc - (tim2 - lastSrc)) * (avgBlkDiffSrc - (tim2 - lastSrc)));
					}
					pktSizeTotalSrc += c->parser.pktHdr->orig_len;
					lastSrc = tim2;
				}
					
				if(foundDst == true)
				{
					if(avgDiffDstSq < -1)
					{
						avgDiffDstSq = 0.000000;
						avgBlkDiffDstSq = 0.000000;
					}
					else
					{
						avgDiffDstSq += ((avgDiffDst - (tim2 - lastDst)) * (avgDiffDst - (tim2 - lastDst)));	
						
						if((tim2 - lastDst) > 1.000000)
							avgBlkDiffDstSq += ((avgBlkDiffDst - (tim2 - lastDst)) * (avgBlkDiffDst - (tim2 - lastDst)));
					}
					
					pktSizeTotalDst += c->parser.pktHdr->orig_len;
					lastDst = tim2;
				}
				
				tim1 = tim2;
				c = (PCAP_FLOW_NODE *)b->flows.getNext((DLL_NODE *)c);
			}

			if(countDiff >= 1)
				avgDiffSq = sqrt(avgDiffSq/countDiff);
			else
				avgDiffSq = 0.000000;

			if(countBlkDiff >= 1)
				avgBlkDiffSq = sqrt(avgBlkDiffSq/countBlkDiff);
			else
				avgBlkDiffSq = 0.000000;
				
			if(countDiffDst >= 1)
				avgDiffDstSq = sqrt(avgDiffDstSq/countDiffDst);
			else
				avgDiffDstSq = 0.000000;

			if(countDiffSrc >= 1)
				avgDiffSrcSq = sqrt(avgDiffSrcSq/countDiffSrc);
			else
				avgDiffSrcSq = 0.000000;

			if(countBlkDiffDst >= 1)
				avgBlkDiffDstSq = sqrt(avgBlkDiffDstSq/countBlkDiffDst);
			else
				avgBlkDiffDstSq = 0.000000;

			if(countBlkDiffSrc >= 1)
				avgBlkDiffSrcSq = sqrt(avgBlkDiffSrcSq/countBlkDiffSrc);
			else
				avgBlkDiffSrcSq = 0.000000;

			if(!csvMode)
			{
				fprintf(outputFile, "Stats For flowId : %d\n", b->flow_id);
				fprintf(outputFile, "Type = %s,  HigherLayerProto = %u,  TOS = 0x%x\n", typeStr, higherLayerProto, tos);
				fprintf(outputFile, "IP1: %s    IP2: %s    Port1: %u    Port2: %u\n", ipStr1, ipStr2, port1, port2);
				fprintf(outputFile, "Start = %s.%06u, End = %s.%06u\n", startTimeStr, startUSecs, endTimeStr, endUSecs);
				fprintf(outputFile, "Data in packets [Total/US/DS] = %u %u %u bytes\n", pktSizeTotal, pktSizeTotalSrc, pktSizeTotalDst);
				fprintf(outputFile, "Pkt count[Total/Us/Ds] = %d %d %d\n", countDiff + 1, countDiffSrc + 1, countDiffDst + 1);
				fprintf(outputFile, "Mean inter-packet time for flow [Total/Us/Ds] = %lf %lf %lf s\n", avgDiff, avgDiffSrc, avgDiffDst);
				fprintf(outputFile, "SD for inter-packet times[Total/Us/Ds] = %lf %lf %lf s\n", avgDiffSq, avgDiffSrcSq, avgDiffDstSq);
				fprintf(outputFile, "Burst count[Total/Us/Ds] = %d %d %d\n", countBlkDiff + 1, countBlkDiffSrc + 1, countBlkDiffDst + 1);				
				fprintf(outputFile, "Mean inter-burst pkttime for flow [Total/Us/Ds] = %lf %lf %lf s\n", avgBlkDiff, avgBlkDiffSrc, avgBlkDiffDst);
				fprintf(outputFile, "SD for inter-burst pkttimes [Total/Us/Ds] = %lf %lf %lf s\n\n", avgBlkDiffSq, avgBlkDiffSrcSq, avgBlkDiffDstSq);
			}
			else
			{
				fprintf(outputFile,
						"%u, %s, %u, 0x%x, %s:%u, %s:%u, %s.%06u, %s.%06u, %u, %u, %u, %d, %d, %d, %lf, %lf, %lf, %lf, %lf, %lf, %d, %d, %d, %lf, %lf, %lf, %lf, %lf, %lf\n",
						b->flow_id,
						typeStr, higherLayerProto, tos, ipStr1, port1, ipStr2, port2, startTimeStr, startUSecs, endTimeStr, endUSecs,
						pktSizeTotal, pktSizeTotalSrc, pktSizeTotalDst,
						countDiff + 1, countDiffSrc + 1, countDiffDst + 1,
						avgDiff, avgDiffSrc, avgDiffDst,
						avgDiffSq, avgDiffSrcSq, avgDiffDstSq,
						countBlkDiff + 1, countBlkDiffSrc + 1, countBlkDiffDst + 1,
						avgBlkDiff, avgBlkDiffSrc, avgBlkDiffDst,
						avgBlkDiffSq, avgBlkDiffSrcSq, avgBlkDiffDstSq);
			}
		}
		else if(b->flows.getListSize() >= eligibilityCriteria && b->flows.getListSize() < 2)
		{
			if(!csvMode)
			{
				fprintf(outputFile, "Stats For flowId : %d\n", b->flow_id);
				fprintf(outputFile, "Type = %s,  HigherLayerProto = %u,  TOS = 0x%x\n", typeStr, higherLayerProto, tos);
				fprintf(outputFile, "IP1: %s    IP2: %s    Port1: %u    Port2: %u\n", ipStr1, ipStr2, port1, port2);
				fprintf(outputFile, "Start = %s.%06u, End = %s.%06u\n", startTimeStr, startUSecs, endTimeStr, endUSecs);
				fprintf(outputFile, "There are less than 2 packets in this flow! [%d]\n\n", b->flows.getListSize());
			}
			else
			{
				fprintf(outputFile, "%u, %s, %u, 0x%x, %s:%u, %s:%u, %s.%06u, %s.%06u\n", 
						b->flow_id, typeStr, higherLayerProto, tos, ipStr1, port1, ipStr2, port2, startTimeStr, startUSecs, endTimeStr, endUSecs);
			}
		}
			
	}
}


int32_t performFlowAnalysis(char *inputFileName, bool automated)
{	
	CBSTree flowIdTree(TRUE, flowDetectCompare, NULL);
	CBSTree orderedFlowIdTree(TRUE, orderedFlowDetectCompare, NULL);
	PCAP_GLOBAL_HDR *globalHdr;
	PCAP_PKT_PARSER *pktP;
	PCAP_FLOWID_TREE_NODE *flowTrNode;
	PCAP_FLOWID_TREE_NODE *tempTrNode;
	PCAP_FLOW_LIST_NODE *flowListNode;
	PCAP_FLOW_NODE *flowNode;
	PCAP_FLOW_NODE *tempFlowNode;
	PCAP_ORDERED_FLOWID_TREE_NODE *pOrderedNode;
	PCAP_ORDERED_FLOWID_TREE_NODE tempOrderedNode;	
	bool newNode = true;
	void *packet;
	void *prevPacket;
	int32_t count = 0, udpCount = 0, tcpCount = 0, tcpStartCount = 0;
	int32_t pktLen;
	int32_t newFlow = 0, foundFlow = 0;
	int32_t flowIdCounter = 0;
	int32_t hits = 0;
	int32_t selector = 0, i;
	char *charPtr;
	
	printf("Starting PCAP Flow Classifier... \n");
	if(parser.openPcap(inputFileName) != OK)
		return -1;

	globalHdr = parser.getGlobalHdr();
	printGlobalHdr(globalHdr);

	if(globalHdr->network == 0x1)
		printf("Ethernet frame format detected\n");
	else if(globalHdr->network == 119)
		printf("IEEE802.11 prism frame format detected\n");
	else
	{
		printf("Unknown frame format\n");
		return -1;
	}
	PCAP_PRINT(0x1)("\nPacket Info to follow..\n");
	while(parser.readNextPkt(&packet, &pktLen) == OK)
	{
		count++;
		flowNode = (PCAP_FLOW_NODE *)parser.allocateMem(sizeof(PCAP_FLOW_NODE));
		assert(flowNode != NULL);

		pktP = &flowNode->parser;
		parser.parsePkt(packet, pktLen, pktP);
		pktP->packet_id = count;

		PCAP_PRINT(0x1)("Packet# %d : ", count);
		if(pktP->type & PKT_TYPE_IP)
		{
			PCAP_PRINT(0x1)("src = ");
			printIpAddr(pktP->ip_hdr->src_addr);
			PCAP_PRINT(0x1)("; dest = ");
			printIpAddr(pktP->ip_hdr->dst_addr);

			if(pktP->type & (PKT_TYPE_TCP | PKT_TYPE_UDP))	
			{
				if(newNode)
				{
					flowTrNode = (PCAP_FLOWID_TREE_NODE *)parser.allocateMem(sizeof(PCAP_FLOWID_TREE_NODE));	
					newNode = false;
				}

				assert(flowTrNode != NULL);
				flowTrNode->parser = *pktP;

				tempTrNode = (PCAP_FLOWID_TREE_NODE *)flowIdTree.findNode((PTR_BS_TREE_NODE)flowTrNode);
				if(tempTrNode == NULL)
				{
					newFlow++;
					flowListNode = (PCAP_FLOW_LIST_NODE *)parser.allocateMem(sizeof(PCAP_FLOW_LIST_NODE));
		
					assert(flowTrNode != NULL);
					assert(flowListNode != NULL);

					flowTrNode->flowList.initCB();
					flowListNode->flows.initCB();
					flowListNode->flow_id = ++flowIdCounter;

					if(pktP->type & PKT_TYPE_TCP)
					{
						if(pktP->tcp_hdr->flags_syn && pktP->tcp_hdr->ack_num == 0)
							flowListNode->tcpFlowState = TCP_FLOW_STATE_STARTING;
						else if(pktP->tcp_hdr->flags_fin)
							flowListNode->tcpFlowState = TCP_FLOW_STATE_FINISHING;
						else
							flowListNode->tcpFlowState = TCP_FLOW_STATE_UNKNOWN;
					}
					else
					{
						flowListNode->tcpFlowState = TCP_FLOW_STATE_NONE;
					}

					flowTrNode->flowList.insertAtTail((DLL_NODE *)flowListNode);
					flowListNode->flows.insertAtTail((DLL_NODE *)flowNode);

					//Insert
					if(flowIdTree.insertNode((PTR_BS_TREE_NODE)flowTrNode) == OK)
					{
						newNode = true;
						pOrderedNode = (PCAP_ORDERED_FLOWID_TREE_NODE *)parser.allocateMem(sizeof(PCAP_ORDERED_FLOWID_TREE_NODE));
						assert(pOrderedNode != NULL);
						pOrderedNode->flow_id = flowIdCounter;
						pOrderedNode->node = flowListNode;
						assert(orderedFlowIdTree.insertNode((PTR_BS_TREE_NODE)pOrderedNode) == OK);
					}
				}
				else
				{
					foundFlow++;
					flowListNode = (PCAP_FLOW_LIST_NODE *)tempTrNode->flowList.getTail();
					assert(flowListNode != NULL);

					assert(flowListNode->flows.getListSize() >= 0);
					tempFlowNode = (PCAP_FLOW_NODE *)flowListNode->flows.getTail();
					assert(tempFlowNode != NULL);

					if(/*(pktP->type & PKT_TYPE_UDP) && */
					   (flowNode->parser.pktHdr->ts_sec - tempFlowNode->parser.pktHdr->ts_sec) > 1800)
					{
						// Its been to long since we received the last packet from this flow.
						// Create a new flow and add the packet there
						hits++;
						newFlow++;
						flowListNode = (PCAP_FLOW_LIST_NODE *)parser.allocateMem(sizeof(PCAP_FLOW_LIST_NODE));	
						flowListNode->flows.initCB();
						flowListNode->flow_id = ++flowIdCounter;
						flowListNode->tcpFlowState = TCP_FLOW_STATE_STARTING;
						tempTrNode->flowList.insertAtTail((DLL_NODE *)flowListNode);
						
						pOrderedNode = (PCAP_ORDERED_FLOWID_TREE_NODE *)parser.allocateMem(sizeof(PCAP_ORDERED_FLOWID_TREE_NODE));
						assert(pOrderedNode != NULL);
						pOrderedNode->flow_id = flowIdCounter;
						pOrderedNode->node = flowListNode;
						assert(orderedFlowIdTree.insertNode((PTR_BS_TREE_NODE)pOrderedNode) == OK);						
					}
					else if((pktP->type & PKT_TYPE_TCP) && pktP->tcp_hdr->flags_syn) 
					{
						if(flowListNode->tcpFlowState == TCP_FLOW_STATE_FINISHING) 
						{
							//printf("Hit @ %u.%u\n", flowNode->parser.pktHdr->ts_sec, flowNode->parser.pktHdr->ts_usec);
							hits++;
							newFlow++;
							flowListNode = (PCAP_FLOW_LIST_NODE *)parser.allocateMem(sizeof(PCAP_FLOW_LIST_NODE));	
							flowListNode->flows.initCB();
							flowListNode->flow_id = ++flowIdCounter;
							flowListNode->tcpFlowState = TCP_FLOW_STATE_STARTING;
							tempTrNode->flowList.insertAtTail((DLL_NODE *)flowListNode);
							
							pOrderedNode = (PCAP_ORDERED_FLOWID_TREE_NODE *)parser.allocateMem(sizeof(PCAP_ORDERED_FLOWID_TREE_NODE));
							assert(pOrderedNode != NULL);
							pOrderedNode->flow_id = flowIdCounter;
							pOrderedNode->node = flowListNode;
							assert(orderedFlowIdTree.insertNode((PTR_BS_TREE_NODE)pOrderedNode) == OK);
						}
					}

					if(pktP->type & PKT_TYPE_TCP)
					{
						if(flowNode->parser.tcp_hdr->flags_fin)
							flowListNode->tcpFlowState = TCP_FLOW_STATE_FINISHING;

						if(flowListNode->tcpFlowState != TCP_FLOW_STATE_FINISHING &&
					   	   flowNode->parser.tcp_hdr->ack_num && flowNode->parser.tcp_hdr->seq_num)
							flowListNode->tcpFlowState = TCP_FLOW_STATE_ONGOING;
					}

					flowListNode->flows.insertAtTail((DLL_NODE *)flowNode);
				}
			}

			if(pktP->type & PKT_TYPE_TCP)
			{
				tcpCount++;
				if(pktP->tcp_hdr->flags_syn && pktP->tcp_hdr->flags_ack)
					tcpStartCount++;

				PCAP_PRINT(0x1)("; TCP : sp=%d, dp=%d", 
						ntohs(pktP->tcp_hdr->src_port),
						ntohs(pktP->tcp_hdr->dst_port));
			}

			if(pktP->type & PKT_TYPE_UDP)
			{
				udpCount++;
				PCAP_PRINT(0x1)("; UDP : sp=%d, dp=%d", 
						ntohs(pktP->udp_hdr->src_port),
						ntohs(pktP->udp_hdr->dst_port));
			}
		}
		else if(pktP->type & PKT_TYPE_PRISM)
		{
		}
		else if(pktP->eth_hdr)
		{
			printMacAddr(*(MACADDR *)pktP->eth_hdr);
			PCAP_PRINT(0x1)(" Not ip.. type = %x\n", pktP->type);
		}

		PCAP_PRINT(0x1)("\n");
		prevPacket = packet;
	}

	printf("Parsing complete. Starting verification phase..\n");
	globalCounter = 0;
	flowIdTree.setTraverse(treeVerify);
	flowIdTree.inOrderTraverse();
	if(globalCounter == (tcpCount + udpCount))
		printf("Verification complete! Everything looks alright..\n\n");

	printf("There are %d packets in pcap file; UDP %d, TCP %d\n", count, udpCount, tcpCount);
	printf("Classified %d flows\n", newFlow);

	do
	{
		if(automated)
		{
			break;
		}
		
		printf("\n");
		printf("Select an option:\n");
		printf("   1. Count flows having packet count >= x\n");
		printf("   2. Print flows having packet count >= x\n");
		printf("   3. Dump packets in flow x to an output file\n");
		printf("   4. Save flow statistics to file - flow x satisfying criteria y\n");
		printf("  99. Exit\n");
		printf("Enter choice: ");
		fflush(stdin);
		scanf("%d", &selector);
		
		switch(selector)
		{
			case 1:
				printf("\nEnter qualifying packet count: ");
				fflush(stdin);
				scanf("%d", &eligibilityCriteria);
				globalCounter = 0;
				orderedFlowIdTree.setTraverse(countEligibleFlows);
				orderedFlowIdTree.inOrderTraverse();
				printf("There are %d flows with more than %d packets\n", globalCounter, eligibilityCriteria);
				break;
				
			case 2:
				printf("Enter qualifying packet count: ");
				fflush(stdin);
				scanf("%d", &eligibilityCriteria);
				orderedFlowIdTree.setTraverse(printEligibleFlows);
				orderedFlowIdTree.inOrderTraverse();
				printf("\n");				
				break;
				
			case 3:
				printf("Enter flowId [1-%d]: ", newFlow);
				fflush(stdin);				
				scanf("%d", &tempOrderedNode.flow_id);
				pOrderedNode = (PCAP_ORDERED_FLOWID_TREE_NODE *)orderedFlowIdTree.findNode((PTR_BS_TREE_NODE)&tempOrderedNode);
				if(pOrderedNode == NULL)
				{
					printf("FlowId %u could not be found\n", tempOrderedNode.flow_id);
					break;
				}
				printf("Enter output filename: \n");
				fflush(stdin);
				scanf("%s", outputFileName);
				writeFlowToFile(pOrderedNode);
				break;
				
			case 4:
				printf("Enter flowId [1-%d; -2 for all flows]: ", newFlow);
				fflush(stdin);				
				scanf("%d", &selectedFlowId);

				if(selectedFlowId == 0xFFFFFFFE)
				{
					printf("Enter qualifying packet count: ");
					fflush(stdin);				
					scanf("%d", &eligibilityCriteria);
				}
				else
				{
					eligibilityCriteria = 0;
					tempOrderedNode.flow_id = selectedFlowId;
					pOrderedNode = (PCAP_ORDERED_FLOWID_TREE_NODE *)orderedFlowIdTree.findNode((PTR_BS_TREE_NODE)&tempOrderedNode);
					if(pOrderedNode == NULL)
					{
						printf("FlowId %u could not be found\n", tempOrderedNode.flow_id);
						break;
					}					
				}
					
				printf("Enter output filename [stdout for screen]: \n");
				fflush(stdin);
				scanf("%s", outputFileName);
				csvMode = false;
				
				if(strncmp(outputFileName, "stdout", 6) == 0)
				{
					outputFile = stdout;
				}
				else
				{
					outputFile = fopen(outputFileName, "w+");
					if(outputFile == NULL)
						break;

					i = strlen(outputFileName);
					if(i > 4 && strncmp(&outputFileName[i-4], ".csv", 4) == 0)
						csvMode = true;
				}

				if(csvMode)
				{
					fprintf(outputFile,
						"FlowId, FlowType, HigherLevelProto, TOS, EndPoint1, EndPoint2, StartTime, EndTime, TotalBytes, USBytes, DSBytes, NumPkts, NumPktsUS, NumPktsDS, Mean InterPkt, Mean InterPkt US, Mean InterPkt DS, "
						"SD InterPkt, SD InterPkt US, SD InterPkt DS, BurstCount, BurstCountUS, BurstCountDS, Mean InterBurst, Mean InterBurst US, "
						"Mean InterBurst DS, SD InterBurst, SD InterBurst US, SD InterBurst DS\n");
				}
				
				fprintf(outputFile, "\n");
				stillSearching = true;

				if(selectedFlowId != 0xFFFFFFFE)
				{
					eligibilityCriteria = 0;
					printFlowStats(pOrderedNode);
				}
				else
				{
					orderedFlowIdTree.setTraverse(printFlowStats);
					orderedFlowIdTree.inOrderTraverse();
				}
				
				if(outputFile != stdout)
					fclose(outputFile);
					
				csvMode = false;
				outputFile = NULL;
				break;
				
			case 99:
				//exit from loop
				break;
		}
		
	} while(selector != 99 ); //!= exit

	if(automated)
	{
		selectedFlowId = 0xFFFFFFFE;
		eligibilityCriteria = 1;
		csvMode = true;
		
		charPtr = inputFileName;
		for(i = strlen(inputFileName)-1; i > 0; i--)
		{
			if(inputFileName[i] == '\\' || inputFileName[i] == '/')
			{
				charPtr = &(inputFileName[i+1]);
				break;
			}
		}

		strcpy(outputFileName, "../Flows1/");
		strcat(outputFileName, charPtr);

		for(i = strlen(outputFileName)-1; i > 0; i--)
		{
			if(outputFileName[i] == '.')
			{
				outputFileName[i] = '\0';
				break;
			}
		}

		strcat(outputFileName, ".csv");
		
		printf("OutputFileName = %s\n", outputFileName);
		printf("InputFileName = %s\n", inputFileName);		

		outputFile = fopen(outputFileName, "w+");
		if(outputFile == NULL)
		{
			printf("Failed to open file %s\n", outputFileName);
			return -1;			
		}

		if(csvMode)
		{
			fprintf(outputFile,
				"FlowId, FlowType, HigherLevelProto, TOS, EndPoint1, EndPoint2, StartTime, EndTime, TotalBytes, USBytes, DSBytes, NumPkts, NumPktsUS, NumPktsDS, Mean InterPkt, Mean InterPkt US, Mean InterPkt DS, "
				"SD InterPkt, SD InterPkt US, SD InterPkt DS, BurstCount, BurstCountUS, BurstCountDS, Mean InterBurst, Mean InterBurst US, "
				"Mean InterBurst DS, SD InterBurst, SD InterBurst US, SD InterBurst DS\n");
		}
				
		fprintf(outputFile, "\n");
		stillSearching = true;

		orderedFlowIdTree.setTraverse(printFlowStats);
		orderedFlowIdTree.inOrderTraverse();
	
		fclose(outputFile);
					
		csvMode = false;
		outputFile = NULL;
		printf("Completed!!\n\n");
	}
	
	return 0;	
}


int32_t main(int32_t argc, char *argv[])
{
	bool automated = false;
	char *inpFiles[] = 
	{
		(char *)"../../sigcomm08-traces/sigcomm08_wl_2_2008-08-18_10-32_31_2008-08-18_13-33_08_b88796506ceb_40.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-18_10-57_16_2008-08-18_12-10_42_b8879619fd97_64.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-18_14-19_23_2008-08-18_16-58_00_b8879619fd97_64.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-19_11-03_14_2008-08-19_17-52_10_b8879693f167_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-20_10-46_18_2008-08-20_10-46_22_b887966234cf_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-20_10-46_33_2008-08-20_10-54_01_b887966234cf_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-20_10-54_05_2008-08-20_17-43_54_b887967d6666_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_4_2008-08-21_10-52_44_2008-08-21_15-40_16_b88796f1e2c7_52.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_6_2008-08-18_10-38_57_2008-08-18_16-53_35_b88796ca1275_56.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_6_2008-08-19_12-53_20_2008-08-19_14-26_56_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_6_2008-08-19_14-33_56_2008-08-19_17-24_30_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_6_2008-08-20_10-57_46_2008-08-20_17-49_37_b88796f1e2c7_52.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_6_2008-08-21_10-56_43_2008-08-21_11-13_02_b88796f1e2c7_52.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_8_2008-08-19_11-36_24_2008-08-19_17-43_11_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_8_2008-08-20_10-58_06_2008-08-20_10-58_12_b88796a0d626_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_8_2008-08-20_10-58_15_2008-08-20_17-50_06_b88796a13ec7_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_8_2008-08-21_11-10_18_2008-08-21_13-38_58_b887965a5527_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-18_10-40_22_2008-08-18_16-56_32_b88796ca1275_56.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-19_12-50_27_2008-08-19_17-20_20_b8879693f167_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-20_10-45_24_2008-08-20_10-45_25_b887965a5527_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-20_10-46_19_2008-08-20_10-46_28_b887965a5527_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-20_10-46_31_2008-08-20_17-44_57_b887965a5527_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_9_2008-08-21_10-54_20_2008-08-21_15-40_48_b88796a13ec7_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_10_2008-08-18_10-51_42_2008-08-18_16-45_56_b88796f1ad39_48.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_10_2008-08-19_11-22_23_2008-08-19_18-07_56_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_10_2008-08-20_10-50_35_2008-08-20_17-46_56_b88796f1e2c7_52.pcap", 
		(char *)"../../sigcomm08-traces/sigcomm08_wl_10_2008-08-21_11-00_39_2008-08-21_15-39_41_b88796a13ec7_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-18_10-44_12_2008-08-18_16-51_10_b88796f1ad39_48.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-19_13-09_45_2008-08-19_13-09_49_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-19_13-10_42_2008-08-19_19-24_12_b88796010035_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-20_10-51_15_2008-08-20_10-52_05_b88796a0d626_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-20_10-52_48_2008-08-20_17-47_49_b88796a13ec7_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_13_2008-08-21_11-11_44_2008-08-21_15-53_10_b887967d6666_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_14_2008-08-18_10-42_21_2008-08-18_16-52_45_b88796506ceb_40.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_14_2008-08-19_12-59_07_2008-08-19_19-23_39_b8879693f167_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_14_2008-08-20_11-28_46_2008-08-20_17-51_52_b887967d6666_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_14_2008-08-21_10-49_06_2008-08-21_15-45_24_b887967d6666_60.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-18_10-50_47_2008-08-18_11-23_00_b88796f1ad39_48.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-18_11-35_45_2008-08-18_17-01_18_b88796f1ad39_48.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-19_10-57_51_2008-08-19_18-02_54_b8879693f167_149.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-20_11-27_50_2008-08-20_11-27_52_b8879694c8a8_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-20_11-27_54_2008-08-20_11-28_55_b8879694c8a8_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-20_11-29_26_2008-08-20_17-52_37_b887965a5527_36.pcap",
		(char *)"../../sigcomm08-traces/sigcomm08_wl_16_2008-08-21_10-50_15_2008-08-21_15-44_57_b887965a5527_36.pcap"
	};
	
	int32_t fileCount = 44;
	int32_t i = 0;
	
	if (!automated && argc != 2) 
	{
		printf("Usage: %s <pcap filename>\n", argv[0]);
		return -1;
	}

	if(!automated)
	{
		performFlowAnalysis(argv[1], automated);
	}
	else
	{
		for(i = 0; i < fileCount; i++)
		{
			performFlowAnalysis(inpFiles[i], automated);
			parser.closePcap();
		}
	}
}
