#include <cstdio>
#include <cstring>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include "../include/pcap_parser.h"

//Threshold = min scratchpad buffer = 16 MB
#define PCAP_MEM_THRESHOLD (16 * 1024 * 1024)

using namespace std;

CPcapParser :: CPcapParser()
{
	pcapFd = -1;
	pcapGlobalHdr = NULL;
	pcapFileSize = 0;

	pcapDynMem = NULL;
	pcapDynMemFreePtr = NULL;
	pcapDynMemSizeRemain = 0;
}

CPcapParser :: ~CPcapParser()
{
	if(pcapDynMem != NULL)
		free(pcapDynMem);

	pcapDynMem = NULL;
	pcapDynMemFreePtr = NULL;
	pcapDynMemSizeRemain = 0;
	
	if(pcapGlobalHdr != NULL)
		munmap(pcapGlobalHdr, pcapFileSize);

	pcapGlobalHdr = NULL;
	pcapFileSize = 0;

	if(pcapFd != -1)
		close(pcapFd);

	pcapFd = -1;
}


void CPcapParser :: closePcap()
{
	if(pcapDynMem != NULL)
		free(pcapDynMem);

	pcapDynMem = NULL;
	pcapDynMemFreePtr = NULL;
	pcapDynMemSizeRemain = 0;
	
	if(pcapGlobalHdr != NULL)
		munmap(pcapGlobalHdr, pcapFileSize);

	pcapGlobalHdr = NULL;
	pcapFileSize = 0;

	if(pcapFd != -1)
		close(pcapFd);

	pcapFd = -1;	
}
STATUS CPcapParser :: openPcap(char *filename)
{
	struct stat info;
	assert(filename != NULL);
	assert(pcapFd == -1);

	pcapFd = open(filename, O_RDONLY, 0);

	if(pcapFd < 0)
		return ERROR;

	if(fstat(pcapFd, &info) < 0)
		return ERROR;

	if(info.st_size <= 0)
		return ERROR;

	// Allocate memory in one shot for our processing and internal data structs
	// Decide later whether info.st_size is a reasonable estimate
	pcapDynMem = (int8_t *)malloc(info.st_size > PCAP_MEM_THRESHOLD ? info.st_size : PCAP_MEM_THRESHOLD);

	if(pcapDynMem == NULL)
	{
		printf("Error initializing memory allocator\n");
		close(pcapFd);
		pcapFd = -1;
		return ERROR;
	}

	pcapGlobalHdr = (PCAP_GLOBAL_HDR *) mmap(NULL, info.st_size, PROT_READ, 
											 MAP_PRIVATE, pcapFd, 0);

	if (pcapGlobalHdr == MAP_FAILED)
	{
		printf("Error mapping file\n");
		munmap(pcapDynMem, info.st_size);
		pcapDynMem = NULL;
		close(pcapFd);
		pcapFd = -1;
		return ERROR;
	}
	
	pcapFileSize = info.st_size;
	pcapFilePtr = (int8_t *)pcapGlobalHdr;
	pcapLastByte = pcapFilePtr + pcapFileSize;
	pcapFilePtr += sizeof(PCAP_GLOBAL_HDR); /* Point to the first packet */

	pcapDynMemFreePtr = pcapDynMem;
	pcapDynMemSizeRemain = pcapFileSize > PCAP_MEM_THRESHOLD ? pcapFileSize : PCAP_MEM_THRESHOLD;

	return OK;	
}

PCAP_GLOBAL_HDR *CPcapParser :: getGlobalHdr(void)
{
	return pcapGlobalHdr;	
}

STATUS CPcapParser :: readNextPkt(void **buffer, int32_t *buflen)
{
	PCAP_PKT_HDR *pData = (PCAP_PKT_HDR *)pcapFilePtr;
	int32_t len1, len2;

	/* returns PCAP file header + frame */
	assert(buffer != NULL);
	assert(buflen != NULL);
	assert(pcapGlobalHdr != NULL);

	if((pcapFilePtr + sizeof(PCAP_PKT_HDR)) >= pcapLastByte)
		return ERROR;

	//assert(pData->incl_len <= pcapGlobalHdr->snaplen);

	len1 = pData->incl_len + sizeof(PCAP_PKT_HDR);
	len2 = pcapLastByte - pcapFilePtr;
	*buflen = len1 > len2 ? len2 : len1; 
	*buffer = pcapFilePtr;
	pcapFilePtr += *buflen;

	return OK;
}


STATUS CPcapParser :: parsePkt(void *buffer, uint32_t len, 
								PCAP_PKT_PARSER *result)
{
	assert(result != NULL);
	assert(buffer != NULL);
	assert(pcapGlobalHdr != NULL);

	memset(result, 0x00, sizeof(PCAP_PKT_PARSER));

	if(len < sizeof(PCAP_PKT_HDR))
		return ERROR;

	result->pktHdr = (PCAP_PKT_HDR *)buffer;

	len -= sizeof(PCAP_PKT_HDR);
	buffer = ((PCAP_PKT_HDR *)buffer)->data;

	switch(pcapGlobalHdr->network)
	{
		case PCAP_NTYPE_ETHERNET:
		{	
			return parseEthPkt(buffer, len, result);
		}

		case PCAP_NTYPE_PRISM:
		{
			return parsePrismPkt(buffer, len, result);
		}

		default:
		{
		}
	}
}

//inline
STATUS CPcapParser :: parseTcpPkt(void *buffer, uint32_t len,
								PCAP_PKT_PARSER *result)
{
	if(len < sizeof(PCAP_TCP_HDR))
		return ERROR;

	result->type |= PKT_TYPE_TCP;
	result->tcp_hdr = (PCAP_TCP_HDR *)buffer;

	result->data = (uint8_t *)buffer + sizeof(PCAP_TCP_HDR);

	return OK;
}

//inline
STATUS CPcapParser :: parseUdpPkt(void *buffer, uint32_t len,
								PCAP_PKT_PARSER *result)
{
	if(len < sizeof(PCAP_UDP_HDR))
		return ERROR;

	result->type |= PKT_TYPE_UDP;
	result->udp_hdr = (PCAP_UDP_HDR *)buffer;

	result->data = (uint8_t *)buffer + sizeof(PCAP_UDP_HDR);

	return OK;
}

//inline
STATUS CPcapParser :: parseIpPkt(void *buffer, uint32_t len,
								PCAP_PKT_PARSER *result)
{
	uint8_t *ptr = (uint8_t *)buffer;
	uint8_t *temp;
	STATUS status;

	result->type |= PKT_TYPE_IP;
    result->ip_hdr = (PCAP_IP_HDR *)(buffer);

    temp = (uint8_t *)result->ip_hdr + (result->ip_hdr->hdr_len << 2);

    if((temp-ptr) > len)
		return ERROR;

    switch(result->ip_hdr->protocol)
    {
		case IP_HDR_TYPE_TCP:
		{
			status = parseTcpPkt(temp, len - (temp-ptr), result);
			if(status != OK)
			{
				return ERROR;
			}

			break;
		}

		case IP_HDR_TYPE_UDP:
        {
	        status = parseUdpPkt(temp, len - (temp-ptr), result);
            if(status != OK)
			{
				//printf("Parse error!!\n");
				return ERROR;
			}

            break;
        }
    }

	return OK;
}

STATUS CPcapParser :: parseEthPkt(void *buffer, uint32_t len, 
								PCAP_PKT_PARSER *result)
{
	/* Assumes ethernet frame */
	uint8_t *ptr = (uint8_t *)buffer;
	uint8_t *temp = NULL;
	int32_t vlantagsize = 0;
	uint32_t *tempInt;
	uint16_t type_len;
	STATUS status;

	result->type |= PKT_TYPE_ETH;
	result->eth_hdr = (PCAP_ETH_HDR *)ptr;

     if(len <= sizeof(PCAP_ETH_HDR))
		return ERROR;

	type_len = result->eth_hdr->type_len;

parse_start:

	switch(ntohs(type_len))
	{
		case ETH_HDR_TYPE_VLAN_TAG:
		{
			tempInt = (uint32_t *)((uint8_t *)result->eth_hdr + 
											2 * sizeof(MACADDR));

			while(ntohs(*tempInt & 0xFFFF) == ETH_HDR_TYPE_VLAN_TAG)
			{
				vlantagsize+=4;
				tempInt++;
			}

			result->type |= PKT_TYPE_VLAN_TAG;
			type_len = (uint16_t)(*tempInt & 0xFFFF);	
			goto parse_start;
		}
		case ETH_HDR_TYPE_IP:
		{
			temp = (uint8_t *)result->eth_hdr->data + vlantagsize;

			status = parseIpPkt(temp, len - (temp - ptr), result);

			if(status != OK)
				return ERROR;

			break;
		}
	}

	return OK;
}

STATUS CPcapParser :: parsePrismPkt(void *buffer, uint32_t len, 
								PCAP_PKT_PARSER *result)
{
	STATUS status;
	uint8_t *ptr = (uint8_t *)buffer;
	uint8_t *temp;
	PCAP_IEEE80211_LLC *llc = NULL;

    if(len <= (sizeof(PCAP_PRISM_HDR)+ sizeof(PCAP_IEEE80211_FC)))
		return ERROR;

	result->type |= PKT_TYPE_PRISM;
	temp = ptr + sizeof(PCAP_PRISM_HDR);
	
	result->l2_hdr = (PCAP_IEEE80211_FC *)temp;
	temp += sizeof(PCAP_IEEE80211_FC);

	if(result->l2_hdr->type == IEEE_FTYPE_DATA)
	{
    	if(len <= (sizeof(PCAP_PRISM_HDR)+ sizeof(PCAP_IEEE80211_FC) +
					sizeof(PCAP_IEEE80211_LLC)))
			return ERROR;

		llc = (PCAP_IEEE80211_LLC *)temp;
		temp += sizeof(PCAP_IEEE80211_LLC);
	}
	else if(result->l2_hdr->type == IEEE_80211_FTYPE_MGMT)
	{
		//if(result->l2_hdr->subtype == IEEE_80211_FSTYPE_BEACON)
			//printf("Beacon\n");
	}

	if(llc == NULL)
		return OK;

	//printf("Type = %d, subtype = %d, llc = 0x%x dsap = 0x%x\n", result->l2_hdr->type, result->l2_hdr->subtype, ntohs(llc->llc_type), llc->dsap);

	//if(result->
	switch(ntohs(llc->llc_type))
	{
		case ETH_HDR_TYPE_IP:
		{
			status = parseIpPkt(temp, len - (temp-ptr), result);
			if(status != OK)
				return ERROR;

			break;
		}
	}

	return OK;
}

void *CPcapParser :: allocateMem(int32_t size)
{
	assert(size > 0);

	if(size > pcapDynMemSizeRemain)
		return NULL;

	pcapDynMemSizeRemain -= size;
	pcapDynMemFreePtr += size;

	return (pcapDynMemFreePtr - size);
}

